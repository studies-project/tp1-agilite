package services;

import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.junit.*;
import pojos.Article;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.Assert.assertEquals;

public class ServiceTest {

	@Test
	public void testUnit() throws IOException {
		// Exécution de la requête
		URL url = new URL("http://localhost:4567/article/2020-06-03");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();

		// Récupération de la réponse
		String body = IOUtils.toString(connection.getInputStream());

		// Traitement de la réponse
		Gson gson = new Gson();

		Article article = gson.fromJson(body, Article.class);

		assertEquals("2020-06-03", article.getDate());
		assertEquals("The Dance of Venus and Earth",article.getTitle());
		assertEquals("very time Venus passes the Earth, it shows the same face.  This remarkable fact has been known for only about 50 years, ever since radio telescopes have been able to peer beneath Venus' thick clouds and track its slowly rotating surface.  This inferior conjunction -- when Venus and Earth are the closest -- occurs today.  The featured animation shows the positions of the Sun, Venus and Earth between 2010-2023 based on NASA-downloaded data, while a mock yellow 'arm' has been fixed to the ground on Venus to indicate rotation.  The reason for this unusual 1.6-year resonance is the gravitational influence that Earth has on Venus, which surprisingly dominates the Sun's tidal effect.  If Venus could be seen through the Sun's glare today, it would show just a very slight sliver of a crescent. Although previously visible in the evening sky, starting tomorrow, Venus will appear in the morning sky -- on the other side of the Sun as viewed from Earth.   Experts Debate: How will humanity first discover extraterrestrial life?", article.getExplanation());
		assertEquals("https://www.youtube.com/embed/Cd5a5KdPxQc?rel=0", article.getUrl());
	}
}
