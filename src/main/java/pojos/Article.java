package pojos;

public class Article {
	private String date;
	private String title;
	private String explanation;
	private String url;

	public String getDate() { return date; }
	public String getTitle() { return title; }
	public String getExplanation() { return explanation; }
	public String getUrl() { return url; }
}
